#include <iostream>
#include <boost/program_options.hpp>
#include <string>
#include <vector>
#include "tbb/task_scheduler_init.h"

#include "configuration.hh"
#include "pipeline.hh"

#include "encoder/video_reader.hh"
#include "encoder/video_writer.hh"

bool parse_cli(Configuration& conf, int argc, char** argv)
{
  namespace po = boost::program_options;

  // Register all possible options
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("filter,f", po::value<std::vector<std::string>>(), "apply a filter; "
     "choose between grey, reverse, cartoon, drunk, count, color=B,G,R, sepia,"
     " blur[=X], rotate-x, rotate-y, merge, interlace, inlay=B,G,R,"
     " strictinlay=B,G,R")
    ("input-file,i", po::value<std::vector<std::string>>(), "file to treat")
    ("output,o", po::value<std::string>(), "output file")
    ("codec,c", po::value<std::string>(), "output codec")
    ("disable,d", "disable parallelism")
    ;

  // Handle ./prpa file.avi instead of ./prpa --input-file=file.avi
  po::positional_options_description p;
  p.add("input-file", -1);

  // Read command line
  po::variables_map vm;
  try
  {
    po::store(po::command_line_parser(argc, argv).
              options(desc).positional(p).run(), vm);
  }
  catch (po::invalid_command_line_syntax e)
  {
    std::cerr << argv[0] << ": " << e.what() << std::endl;
    return false;
  }
  po::notify(vm);

  // Handle -h and --help
  if (argc <= 1 || vm.count("help"))
  {
    std::cout << desc << "\n";
    return false;
  }

  /* Store configuration */
  if (vm.count("output"))
    conf.output = std::string(vm["output"].as<std::string>());
  else
  {
    std::cerr << "Please provide an output file" << std::endl;
    return false;
  }
  if (vm.count("codec"))
    conf.codec = std::string(vm["codec"].as<std::string>());
  else
    conf.codec = "MP42";

  if (vm.count("disable"))
    conf.para = 1;
  else
    conf.para = -1;

  std::vector<std::string> vs;
  if (vm.count("input-file"))
  {
    vs = vm["input-file"].as<std::vector<std::string>>();
    for (auto it = vs.begin(); it != vs.end(); ++it)
      conf.input.push_back(std::string(*it));
  }
  else
  {
    std::cerr << "Please provide an input file" << std::endl;
	return false;
  }

  if (vm.count("filter"))
  {
    vs = vm["filter"].as<std::vector<std::string>> ();
    for (auto it = vs.begin(); it != vs.end(); ++it)
      conf.add_filter(std::string(*it));
  }

  return true;
}

int main (int argc, char** argv)
{
  Configuration conf = Configuration();

  if (!parse_cli(conf, argc, argv))
    return 1;

  tbb::task_scheduler_init init(conf.para);

  std::list<VideoReader> input_vr = std::list<VideoReader>();
  for (auto it = conf.input.begin(); it != conf.input.end(); ++it)
    input_vr.push_back(VideoReader(*it));
  VideoWriter vw = VideoWriter();
  vw.open(conf.output, conf.codec, input_vr.front().fps_get(),
    input_vr.front().width_get(), input_vr.front().height_get());

  Pipeline pipe(input_vr, vw);

  // Add filters to pipeline
  for (auto filter : conf.filters_get())
  {
    if (!pipe.add_filter(filter.first, filter.second))
    {
      return 2;
    }
  }

  // add filters here
  //pipe.add_filter("grey", std::vector<int>());
  //pipe.add_filter("count", std::vector<int>());
  //pipe.add_filter("cartoon", std::vector<int>());
  //pipe.add_filter("blur", std::vector<int> { 30 });
  //pipe.add_filter("reverse", std::vector<int>());
  //pipe.add_filter("interlace", std::vector<int>());
  //pipe.add_filter("merge", std::vector<int>());
  //pipe.add_filter("inlay", std::vector<int> { 0, 0, 255 });

  pipe.run();

  return 0;
}

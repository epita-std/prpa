#include <utility>
#include <assert.h>
#include "compose.hh"

#include "opencv2/opencv.hpp"

void* Compose::operator()(void* para)
{
  cv::Mat* mat = (cv::Mat*)para;
  std::pair<cv::Mat*, cv::Mat*>* res = new std::pair<cv::Mat*, cv::Mat*>();

  res->first = mat;
  res->second = (cv::Mat*)this->vr_(nullptr);

  return res;
}

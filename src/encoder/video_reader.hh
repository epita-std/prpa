#ifndef VIDEO_READER_HH_
# define VIDEO_READER_HH_

# include <string>
# include "tbb/pipeline.h"
# include "opencv2/opencv.hpp"

class VideoReader: public tbb::filter
{
  public:
    VideoReader(const VideoReader& vr);
    VideoReader(std::string filename);
    int framecount_get();
    int fps_get();
    int width_get();
    int height_get();
    void* operator()(void*);
  private:
    cv::VideoCapture capture_;
    int remaining_;
    int frames_;
};

#endif /* !VIDEO_READER_HH_ */

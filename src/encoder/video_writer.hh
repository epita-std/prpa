#ifndef VIDEO_WRITER_HH_
# define VIDEO_WRITER_HH_

# include <string>
# include "tbb/pipeline.h"

class VideoWriter: public tbb::filter
{
  public:
    VideoWriter() : filter(tbb::filter::serial_in_order) {}
    VideoWriter(const VideoWriter&);
    void* operator()(void* mat);
    bool open(std::string filename, std::string enc,
              int fps, int width, int height);
  private:
    cv::VideoWriter writer_;
};

#endif /* !VIDEO_WRITER_HH_ */

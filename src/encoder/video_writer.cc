#include "opencv2/opencv.hpp"
#include "video_writer.hh"

VideoWriter::VideoWriter(const VideoWriter& vw)
  : filter(tbb::filter::serial_in_order)
{
  this->writer_ = vw.writer_;
}

bool VideoWriter::open(std::string filename, std::string enc,
                        int fps, int width, int height)
{
  if (enc.length() != 4)
    return false;

  return this->writer_.open(filename,
    CV_FOURCC(enc[0], enc[1], enc[2], enc[3]), fps, { width, height } );
}

void* VideoWriter::operator()(void* para)
{
  cv::Mat* mat = (cv::Mat*)para;
  if (this->writer_.isOpened())
    this->writer_.write(*mat);

  delete mat;
  return nullptr;
}

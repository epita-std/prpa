#ifndef VIDEO_HH_
# define VIDEO_HH_

# include <string>
# include "tbb/concurrent_vector.h"
# include "opencv2/opencv.hpp"

class Video
{
  public:
    void close();
    void copy(Video& v);
  protected:
    tbb::concurrent_vector<cv::Mat*> images_;
};

#endif /* !VIDEO_HH_ */

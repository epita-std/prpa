#include "video.hh"

void Video::copy(Video& v)
{
  for (auto it = v.images_.begin(); it != v.images_.end(); ++it)
    this->images_.push_back(new cv::Mat(**it));
}

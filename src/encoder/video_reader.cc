#include "opencv2/opencv.hpp"
#include "video_reader.hh"

VideoReader::VideoReader(std::string filename)
  : filter(tbb::filter::serial_in_order), capture_(filename)
{
  this->frames_ = framecount_get();
  this->remaining_ = this->frames_;
}

VideoReader::VideoReader(const VideoReader& vr)
  : filter(tbb::filter::serial_in_order)
{
  this->capture_ = vr.capture_;
  this->frames_ = framecount_get();
  this->remaining_ = this->frames_;
}

void* VideoReader::operator()(void* ign)
{
  (void)ign;

  static int curr_perc = 0;

  int perc = (this->frames_ - this->remaining_) * 100 / this->frames_;
  if (perc % 10 == 0 && perc > curr_perc)
  {
    curr_perc = perc;
    std::cout << perc << "%... ";
    std::flush(std::cout);
  }

  cv::Mat retrieved;
  if (this->remaining_ > 0)
  {
    this->capture_.read(retrieved);
    cv::Mat* res = new cv::Mat;
    *res = retrieved.clone();
    this->remaining_--;
    return res;
  }
  else
    return nullptr;
}

int VideoReader::framecount_get()
{
  if (this->capture_.isOpened())
    return (int)this->capture_.get(CV_CAP_PROP_FRAME_COUNT);
  else
    return -1;
}

int VideoReader::fps_get()
{
  if (this->capture_.isOpened())
    return (int)this->capture_.get(CV_CAP_PROP_FPS);
  else
    return -1;
}

int VideoReader::width_get()
{
  if (this->capture_.isOpened())
    return (int)this->capture_.get(CV_CAP_PROP_FRAME_WIDTH);
  else
    return -1;
}

int VideoReader::height_get()
{
  if (this->capture_.isOpened())
    return (int)this->capture_.get(CV_CAP_PROP_FRAME_HEIGHT);
  else
    return -1;
}


#ifndef PIPELINE_HH_
# define PIPELINE_HH_

# include <string>
# include <list>
# include <vector>
# include "opencv2/opencv.hpp"
# include "tbb/pipeline.h"

# include "encoder/video.hh"
# include "encoder/video_reader.hh"
# include "encoder/video_writer.hh"

# define CHUNK_SIZE 16

class Pipeline
{
  public:
    Pipeline(std::list<VideoReader> input, VideoWriter output);
    bool add_filter(std::string filter, std::vector<int> args);
    void run();
  private:
    std::list<tbb::filter_t<cv::Mat*, cv::Mat*>> filters_;
    std::list<VideoReader> input_;
    std::list<VideoReader>::const_iterator input_it_;
    VideoWriter output_;
    tbb::pipeline pipeline_;
};

#endif /* !PIPELINE_HH_ */

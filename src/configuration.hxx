#ifndef CONFIGURATION_HXX_
# define CONFIGURATION_HXX_

std::list<std::pair<std::string, std::vector<int> > >
Configuration::filters_get()
{
  return filters_;
}

#endif /* !CONFIGURATION_HXX_ */

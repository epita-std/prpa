#include <string>
#include <list>
#include <vector>
#include <iostream>
#include "tbb/pipeline.h"

#include "pipeline.hh"
#include "compose.hh"
#include "filterSample/grey.hh"
#include "filterSample/cartoon.hh"
#include "filterSample/blur.hh"
#include "filterSample/reverse.hh"
#include "filterSample/merge.hh"
#include "filterSample/interlace.hh"
#include "filterSample/inlay.hh"
#include "filterSample/count.hh"
#include "filterSample/color.hh"
#include "filterSample/rotate.hh"
#include "filterSample/drunk.hh"

Pipeline::Pipeline(std::list<VideoReader> input, VideoWriter output)
  : input_(input), output_(output)
{
  this->input_it_ = this->input_.begin();
  this->pipeline_.add_filter(this->input_.front());
  this->input_it_++;
}

bool Pipeline::add_filter(std::string filter, std::vector<int> args)
{
  if (filter == "grey" || filter == "gray")
    this->pipeline_.add_filter(*(new Grey()));
  else if (filter == "reverse" || filter == "inverse")
    this->pipeline_.add_filter(*(new Reverse()));
  else if (filter == "cartoon")
    this->pipeline_.add_filter(*(new Cartoon()));
  else if (filter == "count")
    this->pipeline_.add_filter(*(new Count()));
  else if (filter == "drunk")
    this->pipeline_.add_filter(*(new Drunk()));
  else if (filter == "rotate-h")
    this->pipeline_.add_filter(*(new Rotate(0)));
  else if (filter == "rotate-v")
    this->pipeline_.add_filter(*(new Rotate(2)));
  else if (filter == "color" && args.size() >= 3)
  {
    if (args.size() < 3)
    {
      std::cerr << "Missing parameters for filter color" << std::endl;
      return false;
    }
    else
      this->pipeline_.add_filter(*(new Color(cv::Vec3b {
              (unsigned char) args[0],
              (unsigned char) args[1],
              (unsigned char) args[2]
            })));
  }
  else if (filter == "sepia")
  {
    this->pipeline_.add_filter(*(new Grey()));
    this->pipeline_.add_filter(*(new Color(cv::Vec3b { 40, 74, 130 })));
  }
  else if (filter == "blur")
  {
    if (args.size() >= 1)
      this->pipeline_.add_filter(*(new Blur(args[0])));
    else
      this->pipeline_.add_filter(*(new Blur()));
  }
  else if (filter == "merge")
  {
    if (this->input_it_ != this->input_.end())
    {
      this->pipeline_.add_filter(*(new Compose(*this->input_it_)));
      this->input_it_++;
      this->pipeline_.add_filter(*(new Merge()));
    }
    else
    {
      std::cerr << "Missing additional input file argument for filter merge"
        << std::endl;
      return false;
    }
  }
  else if (filter == "interlace")
  {
    if (this->input_it_ != this->input_.end())
    {
      this->pipeline_.add_filter(*(new Compose(*this->input_it_)));
      this->input_it_++;
      this->pipeline_.add_filter(*(new Interlace()));
    }
    else
    {
      std::cerr << "Missing additional input file argument for filter "
       "interlace" << std::endl;
      return false;
    }
  }
  else if (filter == "inlay")
  {
    if (this->input_it_ == this->input_.end())
    {
      std::cerr << "Missing additional input file argument for filter inlay"
        << std::endl;
      return false;
    }
    else if (args.size() < 3)
    {
      std::cerr << "Missing parameters for filter inlay"
        << std::endl;
      return false;
    }
    else
    {
      this->pipeline_.add_filter(*(new Compose(*this->input_it_)));
      this->input_it_++;
      this->pipeline_.add_filter(
        *(new Inlay(cv::Vec3b {
              (unsigned char) args[0],
              (unsigned char) args[1],
              (unsigned char) args[2]
            }, 0)));
    }
  }
  else if (filter == "strictinlay" && args.size() >= 3)
  {
    if (this->input_it_ == this->input_.end())
    {
      std::cerr << "Missing additional input file argument for filter "
        "strictinlay" << std::endl;
      return false;
    }
    else if (args.size() < 3)
    {
      std::cerr << "Missing parameters for filter strictinlay" << std::endl;
      return false;
    }
    else
    {
      this->pipeline_.add_filter(*(new Compose(*this->input_it_)));
      this->input_it_++;
      this->pipeline_.add_filter(
        *(new Inlay(cv::Vec3b {
              (unsigned char) args[0],
              (unsigned char) args[1],
              (unsigned char) args[2]
            }, 1)));
    }
  }
  else
  {
    std::cerr << ": Unknown filter `"
      << filter << "'" << std::endl;
    return false;
  }

  return true;
}

void Pipeline::run()
{
  std::cout << "Applying video filter chain: ";
  std::flush(std::cout);
  this->pipeline_.add_filter(this->output_);
  this->pipeline_.run(CHUNK_SIZE);
  std::cout << "Done!" << std::endl;
}

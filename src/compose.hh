#ifndef COMPOSE_HH_
# define COMPOSE_HH_

# include "tbb/pipeline.h"
# include "encoder/video_reader.hh"

class Compose: public tbb::filter
{
  public:
    Compose(VideoReader vr): filter(tbb::filter::serial_in_order), vr_(vr) {}
    void* operator()(void*);
  private:
    VideoReader vr_;
};

#endif /* COMPOSE_HH_ */

#include "configuration.hh"
#include <sstream>
#include <boost/tokenizer.hpp>

void
Configuration::add_filter(const std::string param)
{
  std::pair<std::string, std::vector<int> > pair;

  boost::char_separator<char> sep(",;:. =");
  boost::tokenizer<boost::char_separator<char> > tokens(param, sep);

  for (auto tok : tokens)
  {
    if (pair.first.empty())
      pair.first = tok;
    else
    {
      int p = -1;
      std::stringstream ss (tok);

      ss >> p;
      pair.second.push_back(p);
    }
  }

  filters_.push_back (pair);
}

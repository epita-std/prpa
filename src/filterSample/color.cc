#include "color.hh"
#include "opencv2/opencv.hpp"

void* Color::operator()(void* para)
{
  cv::Mat* mat = (cv::Mat*)para;

  if (mat == nullptr)
    return nullptr;

  for (size_t i = 0; i < mat->rows; ++i)
    for (size_t j = 0; j < mat->cols; ++j)
    {
      mat->at<cv::Vec3b>(i, j)[0] = mat->at<cv::Vec3b>(i, j)[0]
        * this->ref_[0] / 255;

      mat->at<cv::Vec3b>(i, j)[1] = mat->at<cv::Vec3b>(i, j)[1]
        * this->ref_[1] / 255;

      mat->at<cv::Vec3b>(i, j)[2] = mat->at<cv::Vec3b>(i, j)[2]
        * this->ref_[2] / 255;
    }

  return mat;
}

#ifndef BLUR_HH_
# define BLUR_HH_

# include "tbb/pipeline.h"

class Blur: public tbb::filter
{
  public:
    Blur(int size = 15): filter(tbb::filter::parallel), size_(size) {}
    void* operator()(void*);
  private:
    int size_;
};

#endif /* !BLUR_HH_ */

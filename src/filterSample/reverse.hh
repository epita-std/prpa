#ifndef REVERSE_HH_
# define REVERSE_HH_

# include "tbb/pipeline.h"

class Reverse: public tbb::filter
{
  public:
    Reverse(): filter(tbb::filter::parallel) {}
    void* operator()(void*);
};

#endif /* !REVERSE_HH_ */

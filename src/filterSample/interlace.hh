#ifndef INTERLACE_HH_
# define INTERLACE_HH_

# include "tbb/pipeline.h"

class Interlace: public tbb::filter
{
  public:
    Interlace(): filter(tbb::filter::parallel) {}
    void* operator()(void*);
};

#endif /* !INTERLACE_HH_ */

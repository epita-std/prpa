#ifndef CARTOON_HH_
# define CARTOON_HH_

# include "tbb/pipeline.h"

class Cartoon: public tbb::filter
{
  public:
    Cartoon(): filter(tbb::filter::parallel) {}
    void* operator()(void*);
};

#endif /* !CARTOON_HH_ */

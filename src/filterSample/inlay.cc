#include "inlay.hh"
#include "opencv2/opencv.hpp"

static inline bool is_inlay(int strict, cv::Vec3b ref, cv::Vec3b pix)
{
  return (strict && ref == pix) || (!strict &&
    ((ref[0] > 0 && pix[0] - pix[1] > 30 && pix[0] - pix[2] > 30)
      || (ref[1] > 0 && pix[1] - pix[0] > 30 && pix[1] - pix[2] > 30)
      || (ref[2] > 0 && pix[2] - pix[0] > 30 && pix[2] - pix[1] > 30)));
}

void* Inlay::operator()(void* para)
{
  std::pair<cv::Mat*, cv::Mat*>* pair = (std::pair<cv::Mat*, cv::Mat*>*)para;
  cv::Mat* mat = pair->first;

  if (mat == nullptr)
    return nullptr;

  if (pair->second != nullptr)
  {
    for (size_t i = 0; i < mat->rows; ++i)
      for (size_t j = 0; j < mat->cols; ++j)
        if (i < pair->second->rows && j < pair->second->cols
            && is_inlay(this->strict_, this->ref_, mat->at<cv::Vec3b>(i, j)))
          mat->at<cv::Vec3b>(i, j) = pair->second->at<cv::Vec3b>(i, j);
  }

  delete pair->second;
  delete pair;
  return mat;
}

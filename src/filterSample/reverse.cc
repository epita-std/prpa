#include "reverse.hh"

#include "opencv2/opencv.hpp"

void* Reverse::operator()(void* para)
{
  cv::Mat* mat = (cv::Mat*)para;

  if (mat == nullptr)
    return nullptr;

  for (auto it = mat->begin<cv::Vec3b>(); it != mat->end<cv::Vec3b>(); ++it)
  {
    (*it)[0] = ~(*it)[0];
    (*it)[1] = ~(*it)[1];
    (*it)[2] = ~(*it)[2];
  }

  return mat;
}

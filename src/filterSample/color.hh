#ifndef COLOR_HH_
# define COLOR_HH_

# include "tbb/pipeline.h"
# include "opencv2/opencv.hpp"

class Color: public tbb::filter
{
  public:
    Color(cv::Vec3b ref)
      : filter(tbb::filter::parallel), ref_(ref) {}
    void* operator()(void*);
  private:
    cv::Vec3b ref_;
};

#endif /* !COLOR_HH_ */

#ifndef GREY_HH_
# define GREY_HH_

# include "tbb/pipeline.h"

class Grey: public tbb::filter
{
  public:
    Grey(): filter(tbb::filter::parallel) {}
    void* operator()(void*);
};

#endif /* !GREY_HH_ */

#ifndef ROTATE_HH_
# define ROTATE_HH_

# include "tbb/pipeline.h"
# include "opencv2/opencv.hpp"

class Rotate: public tbb::filter
{
  public:
    Rotate(char direction)
      : filter(tbb::filter::parallel), direction_(direction) {}
    void* operator()(void*);
  private:
    char direction_;
};

#endif /* !ROTATE_HH_ */

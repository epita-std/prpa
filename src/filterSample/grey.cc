#include "grey.hh"
#include "opencv2/opencv.hpp"

void* Grey::operator()(void* para)
{
  cv::Mat* mat = (cv::Mat*)para;

  if (mat == nullptr)
    return nullptr;

  /*
  ** RGB2GRAY convert to a black&white matrix that the videowriter refuse, we
  ** have to convert it back to RGB.
  */
  cv::cvtColor(*mat, *mat, CV_RGB2GRAY);
  cv::cvtColor(*mat, *mat, CV_GRAY2RGB);

  return mat;
}

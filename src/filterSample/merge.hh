#ifndef MERGE_HH_
# define MERGE_HH_

# include "tbb/pipeline.h"

class Merge: public tbb::filter
{
  public:
    Merge(): filter(tbb::filter::parallel) {}
    void* operator()(void*);
};

#endif /* !MERGE_HH_ */

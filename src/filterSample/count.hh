#ifndef COUNT_HH_
# define COUNT_HH_

# include "tbb/pipeline.h"

class Count: public tbb::filter
{
  public:
    Count(): filter(tbb::filter::parallel) {}
    void* operator()(void*);
};

#endif /* !COUNT_HH_ */

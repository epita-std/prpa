#ifndef INLAY_HH_
# define INLAY_HH_

# include "tbb/pipeline.h"
# include "opencv2/opencv.hpp"

class Inlay: public tbb::filter
{
  public:
    Inlay(cv::Vec3b ref, int strict)
      : filter(tbb::filter::parallel), ref_(ref), strict_(strict) {}
    void* operator()(void*);
  private:
    cv::Vec3b ref_;
    int strict_;
};

#endif /* !INLAY_HH_ */

#include "drunk.hh"

#include "opencv2/opencv.hpp"

void* Drunk::operator()(void* para)
{
  cv::Mat* mat = (cv::Mat*)para;

  if (mat == nullptr)
    return nullptr;

  if (this->last_ == nullptr)
  {
    this->last_ = new cv::Mat();
    *this->last_ = mat->clone();
  }

  auto it1 = mat->begin<cv::Vec3b>();
  auto it2 = this->last_->begin<cv::Vec3b>();
  while (it1 != mat->end<cv::Vec3b>() && it2 != this->last_->end<cv::Vec3b>())
  {
    (*it2)[0] = (*it1)[0] / 8 + (*it2)[0] * 7 / 8;
    (*it2)[1] = (*it1)[1] / 8 + (*it2)[1] * 7 / 8;
    (*it2)[2] = (*it1)[2] / 8 + (*it2)[2] * 7 / 8;
    (*it1)[0] = (*it2)[0];
    (*it1)[1] = (*it2)[1];
    (*it1)[2] = (*it2)[2];
    ++it1;
    ++it2;
  }

  return mat;
}

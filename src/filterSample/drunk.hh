#ifndef DRUNK_HH_
# define DRUNK_HH_

# include "tbb/pipeline.h"
# include "opencv2/opencv.hpp"

class Drunk: public tbb::filter
{
  public:
    Drunk(): filter(tbb::filter::parallel), last_(nullptr) {}
    void* operator()(void*);
  private:
    cv::Mat* last_;
};

#endif /* !DRUNK_HH_ */

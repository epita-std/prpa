#include "rotate.hh"

void* Rotate::operator()(void* para)
{
  cv::Mat* mat = (cv::Mat*)para;

  if (mat == nullptr)
    return nullptr;

  if (direction_ == 0) // rotate-h
    for (size_t i = 0; i < mat->rows; ++i)
      for (size_t j = 0; j < mat->cols / 2; ++j)
      {
        cv::Vec3b tmp = mat->at<cv::Vec3b>(i, j);
        mat->at<cv::Vec3b>(i, j) = mat->at<cv::Vec3b>(i, mat->cols - j - 1);
        mat->at<cv::Vec3b>(i, mat->cols - j - 1) = tmp;
      }

  else if (direction_ == 2 || direction_ == -2) // rotate-v
    for (size_t i = 0; i < mat->rows / 2; ++i)
      for (size_t j = 0; j < mat->cols; ++j)
      {
        cv::Vec3b tmp = mat->at<cv::Vec3b>(i, j);
        mat->at<cv::Vec3b>(i, j) = mat->at<cv::Vec3b>(mat->rows - i - 1, j);
        mat->at<cv::Vec3b>(mat->rows - i - 1, j) = tmp;
      }

  return mat;
}

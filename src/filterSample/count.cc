#include <string>
#include <thread>
#include "count.hh"
#include "opencv2/opencv.hpp"

void* Count::operator()(void* para)
{
  static __thread int count = 1;
  cv::Mat* mat = (cv::Mat*)para;

  if (mat == nullptr)
    return nullptr;

  cv::putText(*mat, std::to_string(count), cv::Point { 5, 25 },
    cv::FONT_HERSHEY_TRIPLEX, 1, cv::Scalar { 0, 255, 0 }, 1);
  count++;

  return mat;
}

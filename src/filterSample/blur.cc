#include "blur.hh"

#include "opencv2/opencv.hpp"

void* Blur::operator()(void* para)
{
  cv::Mat* mat = (cv::Mat*)para;

  if (mat == nullptr)
    return nullptr;

  cv::blur(*mat, *mat, cv::Size { this->size_, this->size_ });

  return mat;
}

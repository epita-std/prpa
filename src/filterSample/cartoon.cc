#include "cartoon.hh"
#include "opencv2/opencv.hpp"

void* Cartoon::operator()(void* para)
{
  cv::Mat* mat = (cv::Mat*)para;
  cv::Mat tmp = mat->clone();

  if (mat == nullptr)
    return nullptr;

  cv::bilateralFilter(*mat, tmp, -1, 20, 20);

  *mat = tmp;
  return mat;
}

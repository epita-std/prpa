#include <utility>
#include <iostream>
#include "merge.hh"

#include "opencv2/opencv.hpp"

void* Merge::operator()(void* para)
{
  std::pair<cv::Mat*, cv::Mat*>* pair = (std::pair<cv::Mat*, cv::Mat*>*)para;
  cv::Mat* mat = pair->first;

  if (mat == nullptr)
    return nullptr;

  if (pair->second != nullptr)
  {
    for (size_t i = 0; i < mat->rows; ++i)
      for (size_t j = 0; j < mat->cols; ++j)
        if (i < pair->second->rows && j < pair->second->cols)
        {
          mat->at<cv::Vec3b>(i, j)[0] = (mat->at<cv::Vec3b>(i, j)[0]
            + pair->second->at<cv::Vec3b>(i, j)[0]) / 2;
          mat->at<cv::Vec3b>(i, j)[1] = (mat->at<cv::Vec3b>(i, j)[1]
            + pair->second->at<cv::Vec3b>(i, j)[1]) / 2;
          mat->at<cv::Vec3b>(i, j)[2] = (mat->at<cv::Vec3b>(i, j)[2]
            + pair->second->at<cv::Vec3b>(i, j)[2]) / 2;
        }
  }

  delete pair->second;
  delete pair;
  return mat;
}

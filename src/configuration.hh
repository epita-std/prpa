#ifndef CONFIGURATION_HH_
# define CONFIGURATION_HH_

# include <string>
# include <list>
# include <vector>

class Configuration
{
  public:
    std::list<std::string> input;
    std::string output;
    std::string codec;
    int para;
    void add_filter(std::string param);
    inline std::list<std::pair<std::string, std::vector<int> > > filters_get();
  private:
    std::list<std::pair<std::string, std::vector<int> > > filters_;
};

# include "configuration.hxx"

#endif /* !CONFIGURATION_HH_ */
